package com.galvanize;

public class Algorithm {
    public boolean allEqual(String input){

        if (input.equals("")){
            return false;
        }

        char temp =' ';
        String ignoreCase = input.toLowerCase();

        for (int i = 0; i < ignoreCase.length(); i++) {
            if (i == 0) {
                    temp = ignoreCase.charAt(i);
            } else {
                if (ignoreCase.charAt(i) != temp) {
                        return false;
                } else temp = ignoreCase.charAt(i);
            }
        }
        return true;
    }

}
