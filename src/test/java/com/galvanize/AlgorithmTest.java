package com.galvanize;


import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

public class AlgorithmTest {
    @Test
    public void allEqualShouldReturnTrueIfAllCharacterAreEqualIgnoringCase(){
        //SETUP
        Algorithm myAlgorithm = new Algorithm();

        //ENACT
        boolean result = myAlgorithm.allEqual("aAa");
        boolean result2 = myAlgorithm.allEqual("bbBbabbb");
        boolean result3 = myAlgorithm.allEqual("");

        //ASSERT
        assertTrue(result);
        assertFalse(result2);
        assertFalse(result3);

    }
}
